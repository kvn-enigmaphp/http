# Enigma HTTP 

EnigmaPHP Http is a small and simple supportive package for working with http requests and responses.

### Installing EnigmaPHP Http
```bash
composer require enigma-php/http
```

## Usage

### Enums
`RequestMethod` : This class (enum) represents a RequestMethod and therefore contains 9 cases.

**Example :**
```php
RequestMethod::GET

#with value, results in GET
RequestMethod::GET->value
```
`ResponseStatus`: This class (enum) represents a ResponseStatus and therefore contains a large number of cases with a specific response status.

**From each status we can use :**
- The numeric value representation with : ```ResponseStatus::OK->value```
- The Explanation like `OK` with : ```ResponseStatus::OK->getExplanation()```
- The status class like `Successful` with : ```ResponseStatus::OK->getStatusClass()```
